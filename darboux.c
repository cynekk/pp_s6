// fonction de calcul principale : algorithme de Darboux
// (remplissage des cuvettes d'un MNT)
#include <string.h>
#include <mpi.h>

#include "check.h"
#include "type.h"
#include "darboux.h"

// si ce define n'est pas commenté, l'exécution affiche sur stderr la hauteur
// courante en train d'être calculée (doit augmenter) et l'itération du calcul
//#define DARBOUX_PPRINT

#define PRECISION_FLOTTANT 1.e-5

// pour accéder à un tableau de flotant linéarisé (ncols doit être défini) :
#define WTERRAIN(w,i,j) (w[(i)*ncols+(j)])

// calcule la valeur max de hauteur sur un terrain
float max_terrain(const mnt *restrict m)
{
  const int debut_i = 1*m->ncols;
  const int fin_i = (m->nrows + 1) * m->ncols;
  float max = m->terrain[debut_i], max_global;

  for(int i = debut_i ; i < fin_i ; i++)
    if(m->terrain[i] > max)
      max = m->terrain[i];
  CHECK(MPI_Allreduce(&max, &max_global, 1, MPI_FLOAT, MPI_MAX, MPI_COMM_WORLD)==0);
  return(max_global);
}

// initialise le tableau W de départ à partir d'un mnt m
float *init_W(const mnt *restrict m)
{
  const int ncols = m->ncols, nrows = m->nrows;
  int size, rank;
  float *restrict W;
  CHECK(MPI_Comm_rank(MPI_COMM_WORLD, &rank) == 0);
  // Peut contenir toute la matrice
  CHECK((W = malloc((m->nrows_total+1) * ncols * sizeof(float))) != NULL); 
  CHECK(MPI_Comm_size(MPI_COMM_WORLD, &size) == 0);

  // initialisation W
  const float max = max_terrain(m) + 10.;// R: Attention à l'init de max globale
  const int debut_i = 1;
  const int fin_i = nrows + 1;
  #pragma omp parallel for collapse(2) schedule(static) if(OMP_PARALLEL)
  for(int i = debut_i ; i < fin_i ; i++)
  {
    for(int j = 0 ; j < ncols ; j++)
    {
        if((rank==0 && i==debut_i) || (rank==size-1 && i==fin_i-1) || j==0 || j==ncols-1 || TERRAIN(m,i,j) == m->no_data)
          WTERRAIN(W,i,j) = TERRAIN(m,i,j);
        else
          WTERRAIN(W,i,j) = max;
    }
  }

  return(W);
}

// variables globales pour l'affichage de la progression
#ifdef DARBOUX_PPRINT
float min_darboux=9999.; // ça ira bien, c'est juste de l'affichage
// fonction d'affichage de la progression
void dpprint()
{
  static int iter_darboux=0;
  int rank;
  CHECK(MPI_Comm_rank(MPI_COMM_WORLD, &rank) == 0);
  CHECK(MPI_Allreduce(MPI_IN_PLACE, &min_darboux, 1, MPI_FLOAT, MPI_MIN, MPI_COMM_WORLD)==0);
  if(min_darboux != 9999.)
  {
    if(rank == 0)
    {
      fprintf(stderr, "%.3f %d\r", min_darboux, iter_darboux++);
      fflush(stderr);
    }
    min_darboux = 9999.;
  }
  else
    if(rank == 0)
      fprintf(stderr, "\n");
}
#endif


// pour parcourir les 8 voisins :
const int VOISINS[8][2] = {{-1,-1}, {-1,0}, {-1,1}, {0,-1}, {0,1}, {1,-1}, {1,0}, {1,1}};

// cette fonction calcule le nouveau W[i,j] en utilisant Wprec[i,j]
// et ses 8 cases voisines : Wprec[i +/- 1 , j +/- 1],
// ainsi que le MNT initial m en position [i,j]
// inutile de modifier cette fonction (elle est sensible...):
int calcul_Wij(float *restrict W, const float *restrict Wprec, const mnt *m, const int i, const int j)
{
  //const int nrows = m->nrows_total;
  const int ncols = m->ncols;
  int modif = 0;

  // on prend la valeur précédente...
  WTERRAIN(W,i,j) = WTERRAIN(Wprec,i,j);
  // ... sauf si :
  if(WTERRAIN(Wprec,i,j) > TERRAIN(m,i,j))
  {
    // parcourir les 8 voisins haut/bas + gauche/droite
    for(int v=0; v<8; v++)
    {
      const int n1 = i + VOISINS[v][0];
      const int n2 = j + VOISINS[v][1];


      // vérifie qu'on ne sort pas de la grille.
      // ceci est théoriquement impossible, si les bords de la matrice Wprec
      // sont bien initialisés avec les valeurs des bords du mnt
      //CHECK(n1>=1 && n1<nrows+1 && n2>=0 && n2<ncols);
      // Ça marche mieux 

      // si le voisin est inconnu, on l'ignore et passe au suivant
      if(WTERRAIN(Wprec,n1,n2) == m->no_data)
        continue;

      CHECK(TERRAIN(m,i,j)>m->no_data);
      CHECK(WTERRAIN(Wprec,i,j)>m->no_data);
      CHECK(WTERRAIN(Wprec,n1,n2)>m->no_data);

      // il est important de mettre cette valeur dans un temporaire, sinon le
      // compilo fait des arrondis flotants divergents dans les tests ci-dessous
      const float Wn = WTERRAIN(Wprec,n1,n2) + EPSILON;
      if(TERRAIN(m,i,j) >= Wn)
      {
        WTERRAIN(W,i,j) = TERRAIN(m,i,j);
        modif = 1;
        #ifdef DARBOUX_PPRINT
        if(WTERRAIN(W,i,j)<min_darboux)
          min_darboux = WTERRAIN(W,i,j);
        #endif
      }
      else if(WTERRAIN(Wprec,i,j) > Wn)
      {
        WTERRAIN(W,i,j) = Wn;
        modif = 1;
        #ifdef DARBOUX_PPRINT
        if(WTERRAIN(W,i,j)<min_darboux)
          min_darboux = WTERRAIN(W,i,j);
        #endif
      }
    }
  }
  return(modif);
}

static void Send_recv(float *restrict W, int i, int ncols, int dest, MPI_Comm comm)
{
  CHECK(MPI_Ssend(&W[(i-1)*ncols], ncols, MPI_FLOAT, dest, 0, comm)==0);
  CHECK(MPI_Recv(&W[(i)*ncols], ncols, MPI_FLOAT, dest, 0, comm, NULL)==0);
}

static void Recv_send(float *restrict W, int i, int ncols, int src, MPI_Comm comm)
{
  CHECK(MPI_Recv(&W[(i-1)*ncols], ncols, MPI_FLOAT, src, 0, comm, NULL)==0);
  CHECK(MPI_Ssend(&W[i*ncols], ncols, MPI_FLOAT, src, 0, comm)==0);
}
/*****************************************************************************/
/*           Fonction de calcul principale - À PARALLÉLISER                  */
/*****************************************************************************/
// applique l'algorithme de Darboux sur le MNT m, pour calculer un nouveau MNT
mnt *darboux(const mnt *restrict m)
{
  const int ncols = m->ncols;
  const int nrows = m->nrows;
  int size, rank;
  const int debut_i = 1;
  const int fin_i = nrows + 1;

  // initialisation
  float *restrict W, *restrict Wprec;
  CHECK(MPI_Comm_rank(MPI_COMM_WORLD, &rank) == 0);

  // Peut contenir toute la matrice
  CHECK((W = malloc((m->nrows_total+1) * ncols * sizeof(float))) != NULL);
  Wprec = init_W(m);

  CHECK(MPI_Comm_size(MPI_COMM_WORLD, &size) == 0);

  // calcul : boucle principale
  int modif = 1;
  while(modif)
  {
    modif = 0; // sera mis à 1 s'il y a une modification
    // transmission des lignes
    // Les processus pairs envoient en premier, puis recoive. Inverse pour impair.
    if(rank % 2 == 0)
    {
      if(rank != size-1) // Ici, le dernier proc n'envoie pas sa dernière ligne
        Send_recv(Wprec, fin_i, ncols, rank+1, MPI_COMM_WORLD);
    }
    else
      Recv_send(Wprec, debut_i, ncols, rank-1, MPI_COMM_WORLD);

    if (rank != 0)  // Le premier proc n'envoie pas sa première ligne
    {
      if(rank % 2 == 1)
      {
        if(rank != size-1)  // Le dernier proc n'a plus à échanger
          Send_recv(Wprec, fin_i, ncols, rank+1, MPI_COMM_WORLD);
      }
      else
        Recv_send(Wprec, debut_i, ncols, rank-1, MPI_COMM_WORLD);
    }
    // calcule le nouveau W fonction de l'ancien (Wprec) en chaque point [i,j]
    for(int i=debut_i; i<fin_i; i++)
    {
      for(int j=0; j<ncols; j++)
      {
        // calcule la nouvelle valeur de W[i,j]
        // en utilisant les 8 voisins de la position [i,j] du tableau Wprec
        modif |= calcul_Wij(W, Wprec, m, i, j);
      }
    }

      #ifdef DARBOUX_PPRINT
      dpprint();
      #endif

      // échange W et Wprec
      // sans faire de copie mémoire : échange les pointeurs sur les deux tableaux
      float *tmp = W;
      W = Wprec;
      Wprec = tmp;

      CHECK(MPI_Allreduce(MPI_IN_PLACE, &modif, 1, MPI_INT, MPI_BOR, MPI_COMM_WORLD)==0);
    }
    // fin du while principal


  // fin du calcul, le résultat se trouve dans W
  free(Wprec);
  // crée la structure résultat et la renvoie
  mnt *res;
  CHECK((res=malloc(sizeof(*res))) != NULL);
  memcpy(res, m, sizeof(*res));
  res->terrain = W;
  return(res);
}


// fonctions d'entrée/sortie

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <mpi.h>

#include "check.h"
#include "type.h"
#include "io.h"

#define MIN(A,B) ((A) > (B) ? (B) : (A))
#define RELU(A) ((A) > 0 ? (A) : 0)

/**
 * @brief Scatter avec taille variable sur les derniers processus.
 *
 * Les derniers processus ont supposément une taille différente des autre. Le 
 * nombre de lignes de @p m est modifié.
 * Mais si le nombre de processus exède le nombre de ligne, le programme ne
 * fonctionnera pas.
 * @param m mnt à scatter.
 * @param rank Rang dans les processsus.
 * @param size Nombre de processus courant.
 */
static void M_Scatter(mnt *m, int rank, int size)
{
  const int nc = m->ncols;
  const int nr = m->nrows;
  int lignes_proc = nr/size;
  int *sendcnt = NULL;
  int *displs = NULL;
  CHECK((sendcnt = malloc(sizeof(int[size]))) != NULL);
  CHECK((displs = malloc(sizeof(int[size]))) != NULL);

  if(nr % size == 0)
  {
    #pragma omp parallel for schedule(static) if(OMP_PARALLEL)
    for(int i = 0; i < size; i++)
    {
      sendcnt[i] = nc * lignes_proc;
      displs[i] = i * nc * lignes_proc;
    }
  }
  else
  {
    int nb_lignes = nr;
    int empl = 0;
    if((lignes_proc+1)*(size-1) <= nr)
      lignes_proc = lignes_proc + 1;
    for(int i = 0; i < size; i++, nb_lignes -= lignes_proc)
    {
      sendcnt[i] = nc * MIN(RELU(nb_lignes), lignes_proc);
      displs[i] = empl;
      empl += sendcnt[i];
    }
    if(nb_lignes>0)
      sendcnt[size-1] += nc * nb_lignes;
  }

  // Une ligne vide est laissé au début pour tous
  CHECK(MPI_Scatterv(&m->terrain[1*nc], sendcnt, displs, MPI_FLOAT, &m->terrain[1*nc], sendcnt[rank], MPI_FLOAT, 0, MPI_COMM_WORLD)==0);

  m->nrows = sendcnt[rank]/nc;
  free(sendcnt);
  free(displs);
}

mnt *mnt_read(char *fname, double *time_scatter)
{
  mnt *m = NULL;
  FILE *f = NULL;
  int rank, size;
  double begin_scatter, end_scatter;

  CHECK(MPI_Comm_rank(MPI_COMM_WORLD, &rank) == 0);
  CHECK(MPI_Comm_size(MPI_COMM_WORLD, &size) == 0);
  CHECK((m = malloc(sizeof(*m))) != NULL);
  
  if(rank == 0) 
  {
    CHECK((f = fopen(fname, "r")) != NULL);

    CHECK(fscanf(f, "%d", &m->ncols) == 1);
    CHECK(fscanf(f, "%d", &m->nrows) == 1);
    CHECK(fscanf(f, "%f", &m->xllcorner) == 1);
    CHECK(fscanf(f, "%f", &m->yllcorner) == 1);
    CHECK(fscanf(f, "%f", &m->cellsize) == 1);
    CHECK(fscanf(f, "%f", &m->no_data) == 1);
  }
  CHECK(MPI_Bcast(&m->ncols, 1, MPI_INT, 0, MPI_COMM_WORLD) == 0);
  CHECK(MPI_Bcast(&m->nrows, 1, MPI_INT, 0, MPI_COMM_WORLD) == 0);
  CHECK(MPI_Bcast(&m->no_data, 1, MPI_FLOAT, 0, MPI_COMM_WORLD) == 0);
  // Première ligne vide
  CHECK((m->terrain = malloc(m->ncols * (1+m->nrows) * sizeof(float))) != NULL);
  m->nrows_total = m->nrows;

  if(rank == 0)
    // La première ligne est vide.
    for(int i = 1*m->ncols; i < m->ncols * (1+m->nrows) ; i++)
      CHECK(fscanf(f, "%f", &m->terrain[i]) == 1);

  begin_scatter = MPI_Wtime();
  M_Scatter(m, rank, size);
  end_scatter = MPI_Wtime();

  if(rank == 0)
    CHECK(fclose(f) == 0);
  *time_scatter = end_scatter - begin_scatter;
  return(m);
}

/**
 * @brief Gather avec taille variable sur les derniers processus.
 *
 * Les derniers processus ont supposément une taille différente des autre. Le 
 * nombre de lignes de @p m est modifié.
 * Mais si le nombre de processus exède le nombre de ligne, le programme ne
 * fonctionnera pas.
 * @param m mnt à modifier.
 * @param rank Rang dans les processsus.
 * @param size Nombre de processus courant.
 */
static void M_Gather(mnt *m, int rank, int size)
{
  const int nc = m->ncols;
  const int nr = m->nrows_total;
  int lignes_proc = nr/size;
  int *sendcnt = NULL;
  int *displs = NULL;
  CHECK((sendcnt = malloc(sizeof(int[size]))) != NULL);
  CHECK((displs = malloc(sizeof(int[size]))) != NULL);

  if(nr % size == 0)
  {
    #pragma omp parallel for schedule(static) if(OMP_PARALLEL)
    for(int i = 0; i < size; i++)
    {
      sendcnt[i] = nc * lignes_proc;
      displs[i] = i * nc * lignes_proc;
    }
  }
  else
  {
    int nb_lignes = nr;
    int empl = 0;
    if((lignes_proc+1)*(size-1) <= nr)
      lignes_proc = lignes_proc + 1;
    for(int i = 0; i < size; i++, nb_lignes -= lignes_proc)
    {
      sendcnt[i] = nc * MIN(RELU(nb_lignes), lignes_proc);
      displs[i] = empl;
      empl += sendcnt[i];
    }
    if(nb_lignes>0)
      sendcnt[size-1] += nc * nb_lignes;
  }

  CHECK(MPI_Gatherv(&m->terrain[1*nc], sendcnt[rank], MPI_FLOAT, &m->terrain[1*nc], sendcnt, displs, MPI_FLOAT, 0, MPI_COMM_WORLD)==0);

  m->nrows = m->nrows_total;
  free(sendcnt);
  free(displs);
}

void mnt_write(mnt *m, FILE *f, double *time_gather)
{
  CHECK(f != NULL);
  int rank, size;
  double begin_gather, end_gather;
  CHECK(MPI_Comm_rank(MPI_COMM_WORLD, &rank) == 0);
  CHECK(MPI_Comm_size(MPI_COMM_WORLD, &size) == 0);

  begin_gather = MPI_Wtime();
  M_Gather(m, rank, size);
  end_gather = MPI_Wtime();
  *time_gather = end_gather - begin_gather;
  if(rank != 0)
    return;

  fprintf(f, "%d\n", m->ncols);
  fprintf(f, "%d\n", m->nrows);
  fprintf(f, "%.2f\n", m->xllcorner);
  fprintf(f, "%.2f\n", m->yllcorner);
  fprintf(f, "%.2f\n", m->cellsize);
  fprintf(f, "%.2f\n", m->no_data);

  for(int i = 1 ; i < m->nrows+1 ; i++)
  {
    for(int j = 0 ; j < m->ncols ; j++)
    {
      fprintf(f, "%.2f ", TERRAIN(m,i,j));
    }
    fprintf(f, "\n");
  }
}

void mnt_write_lakes(mnt *m, mnt *d, FILE *f)
{
  CHECK(f != NULL);
  int rank, size;
  CHECK(MPI_Comm_rank(MPI_COMM_WORLD, &rank) == 0);
  CHECK(MPI_Comm_size(MPI_COMM_WORLD, &size) == 0);

  M_Gather(m, rank, size);
  if(rank != 0)
    return;

  for(int i = 1 ; i < m->nrows+1 ; i++)
  {
    for(int j = 0 ; j < m->ncols ; j++)
    {
      const float dif = TERRAIN(d,i,j)-TERRAIN(m,i,j);
      fprintf(f, "%c", (dif>1.)?'#':(dif>0.)?'+':'.' );
    }
    fprintf(f, "\n");
  }
}


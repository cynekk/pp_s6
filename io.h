#ifndef __IO_H__
#define __IO_H__

#include "type.h"

mnt *mnt_read(char *fname, double *time_scatter);
void mnt_write(mnt *m, FILE *f, double *time_gather);
void mnt_write_lakes(mnt *m, mnt *d, FILE *f);

#endif

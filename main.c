// programme principal
#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <sys/time.h>
#include <mpi.h>

#include "type.h"
#include "io.h"
#include "darboux.h"

#define DIFFTEMPS(a,b) (((b).tv_sec - (a).tv_sec) + ((b).tv_usec - (a).tv_usec)/1e6)

int main(int argc, char **argv)
{
  mnt *m, *d;
  int err;
  int rank, size;
  double time_scatter, time_gather;
  struct timeval tv_init, tv_begin, tv_end;
  struct timeval tv_end_gather;

  if(argc < 2)
  {
    fprintf(stderr, "Usage: %s <input filename> [<output filename>]\n", argv[0]);
    exit(1);
  }

  err = MPI_Init(&argc, &argv);
  if (err != 0) {
    fprintf(stderr, "MPI_Init failed\n");
    exit(1);
  }

  MPI_Comm_rank(MPI_COMM_WORLD, &rank);
  MPI_Comm_size(MPI_COMM_WORLD, &size);

  // READ INPUT
  gettimeofday( &tv_init, NULL);
  m = mnt_read(argv[1], &time_scatter);

  // COMPUTE
  gettimeofday( &tv_begin, NULL);
  d = darboux(m);
  gettimeofday( &tv_end, NULL);

  // WRITE OUTPUT
  FILE *out;
  if(argc == 3 && rank == 0)
    out = fopen(argv[2], "w");
  else
    out = stdout;

  mnt_write(d, out, &time_gather);
  gettimeofday( &tv_end_gather, NULL);

  if(argc == 3)
  {
    if(rank == 0)
      fclose(out);
  }
  else
    mnt_write_lakes(m, d, stdout);

  // free
  free(m->terrain);
  free(m);
  free(d->terrain);
  free(d);

  double total = DIFFTEMPS(tv_init,tv_begin) + DIFFTEMPS(tv_begin, tv_end)          
    + DIFFTEMPS(tv_end,tv_end_gather);

  if(rank == 0)
    fprintf(stderr, "Init : %lfs, Compute : %lfs, End : %lfs\n"
        "Total : %lfs, Communication : %lfs\n",
        DIFFTEMPS(tv_init,tv_begin),
        DIFFTEMPS(tv_begin,tv_end),
        DIFFTEMPS(tv_end, tv_end_gather),
        total, time_scatter + time_gather);

  err = MPI_Finalize();
  if (err != 0) {
    fprintf(stderr, "MPI_Finalize failed\n");
    exit(1);
  }
  return(0);
}

